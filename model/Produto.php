<?php

class Produto {

    private $nome;
    private $titulo;
    private $descricao;
    private $resumo;

    function getNome() {
        return $this->nome;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getResumo() {
        return $this->resumo;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setResumo($resumo) {
        $this->resumo = $resumo;
    }

    public function load($id) {

              $con = new PDO("mysql:host=localhost;dbname=trabalho", "arch", "4m08ici");
        $stmt = $con->prepare("SELECT * FROM produto WHERE id_produto = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_produto, $row->nome, $row->titulo, $row->descricao, $row->resumo, $row->preco, $row->local, $row->tipo);
            }
        }
        return $results;
    }

    public function mostrarTodosProdutos() {

              $con = new PDO("mysql:host=localhost;dbname=trabalho", "arch", "4m08ici");
        $stmt = $con->prepare("SELECT * FROM produto");
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_produto, $row->nome, $row->titulo, $row->descricao, $row->resumo, $row->preco, $row->local, $row->tipo);
            }
        }
        return $results;
    }

}
