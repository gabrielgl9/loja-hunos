<?php

class Usuario {

    private $login;
    private $email;
    private $senha;

    function getLogin() {
        return $this->login;
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    public function cadastrarUsuario() {

              $con = new PDO("mysql:host=localhost;dbname=trabalho", "arch", "4m08ici");
        $stmt = $con->prepare("INSERT INTO usuario (nome, email, senha) VALUES(?, ?, ?)");
        $stmt->bindParam(1, $this->login);
        $stmt->bindParam(2, $this->email);
        $stmt->bindParam(3, password_hash($this->senha, PASSWORD_DEFAULT));
        $stmt->execute();
    }

    public function carregarDados() {

              $con = new PDO("mysql:host=localhost;dbname=trabalho", "arch", "4m08ici");
        $stmt = $con->prepare("SELECT * FROM usuario WHERE nome = ?");
        $stmt->bindParam(1, $this->login);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                if (password_verify($this->senha, $row->senha)) {
                    $results[] = array($row->id_usuario, $row->nome, $row->email, $row->senha);
                }
            }
        }
        if (!isset($results)) {
            $results = null;
        }
        return $results;
    }

    public function pesquisarNome($login) {

        $con = new PDO("mysql:host=localhost;dbname=trabalho", "arch", "4m08ici");
        $stmt = $con->prepare("SELECT * FROM usuario where nome = ?");
        $stmt->bindParam(1, $login);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                return false;
            }
        }
        return true;
    }

}
