<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Conexao
 *
 * @author Gabri
 */
class Conexao {
    
    private $servidor = "localhost";
    private $database = "trabalho";
    private $user = "root";
    private $password = "";

    public function conecta() {

        $conn = new PDO("mysql:host=$this->servidor;dbname=$this->database", $this->user, $this->password);
        return $conn;
    }

    public function desconecta(&$conn) {
        $conn = null;
    }

}
