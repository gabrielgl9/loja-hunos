-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03-Set-2018 às 15:20
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trabalho`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id_produto` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `titulo` varchar(40) NOT NULL,
  `descricao` varchar(300) NOT NULL,
  `resumo` varchar(100) NOT NULL,
  `preco` float NOT NULL,
  `local` varchar(100) NOT NULL,
  `tipo` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id_produto`, `nome`, `titulo`, `descricao`, `resumo`, `preco`, `local`, `tipo`) VALUES
(1, 'Arco Huno', 'Arco Huno muito legal e barato', 'Esse arco Huno foi usado na guerra contra os romanos, mesmo sabendo que a batalha nÃ£o foi sangrenta, mas sim funkeira.', 'Olha, eu compraria, pois foi usado pelo xamÃ£ da vila (Pietro).', 2000, 'upload/Arco.jpg', 'b'),
(2, 'Espada usada por atila, o Huno', 'Espada de Viegas, o Huno.', 'Essa Ã© a famosa espada de viegas, o Huno, que foi utilizada na guerra contra os romanos, liderada por Augusto(s) e JecÃ©sar, alÃ©m do mensageiro Daniboy. Ela Ã© Ãºnica e vale muito a pena comprar.', 'NÃ£o HÃ¡ tempo a perder, essa espada Ã© demais, e foi utilizada por Viegas, o Huno.', 1000, 'upload/Espada.jpg', 'def'),
(3, 'Escudo Huno', 'Grande escudo encontrado na guerra', 'Este foi um escudo, que foi encontrado na guerra dos romanos contra os Hunos, sabe-se que o xama da vila, tambem conhecido como pietro, encontrou-o no chao e roubou dos romanos antes de usar o portal secreto, em que salvou Viegas, o Huno e o xama da vila (Pietro)', 'Grande escudo Huno que passou por xama da vila e foi roubado dos exercitos de jecesar ', 10000, 'upload/Escudo.jpg', 'h');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `senha` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome`, `email`, `senha`) VALUES
(1, 'Gabriel Viegas', 'gabrielgl13@hotmail.com', '$2y$10$AuITpwGKN0uhaLBi3YfVs.aKsRbW1W8208NHfSLOlY4ZeoNXprhaa'),
(2, 'Viegod', '123@123', '$2y$10$8KBaS8ICS8RNvns.4w6/QOKd3g2nbzKrasx6avorVxEeUtFpmOaRC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id_produto`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
