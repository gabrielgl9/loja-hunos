<?php
// Mostar erros
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once '../model/Usuario.php';

if (!session_id()) {
    session_start();
}

if (isset($_POST['cadastro'])) {
    UsuarioController::cadastrarUsuario();
    die();
}

if (isset($_POST['autenticar'])) {
    UsuarioController::autenticacarUsuario();
    die();
}

class UsuarioController {

    public static function cadastrarUsuario() {


        $login = filter_var($_POST['login'], FILTER_SANITIZE_STRING);
        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $senha = filter_var($_POST['senha'], FILTER_SANITIZE_STRING);


        $usuarioModel = new Usuario();
        if ($usuarioModel->pesquisarNome($login)) {

            $usuarioModel->setLogin($login);
            $usuarioModel->setEmail($email);
            $usuarioModel->setSenha($senha);
            $usuarioModel->cadastrarUsuario();

            $_SESSION['usuario'] = $usuarioModel->carregarDados();
            echo 'aaa';
            header("Location: ../telaInicial.php");
        } else {
            $_SESSION['sweet'] = "Erro ao cadastrar!";
            header("Location: ../telaCadastrar.php?resultado=error");
        }
    }

    public static function autenticacarUsuario() {

        $login = filter_var($_POST['login'], FILTER_SANITIZE_STRING);
        $senha = filter_var($_POST['senha'], FILTER_SANITIZE_STRING);

        $usuarioModel = new Usuario();
        $usuarioModel->setLogin($login);
        $usuarioModel->setSenha($senha);
        $arr = $usuarioModel->carregarDados();
        if (!empty($arr)) {
            $_SESSION['usuario'] = $arr;
            header("Location: ../telaInicial.php");
        } else {
            $_SESSION['sweet'] = "Erro ao realizar a autenticação!";
            header("Location: ../telaLogin.php?resultado=error");
        }
    }

}
