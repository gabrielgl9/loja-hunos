<?php

session_start();

if (file_exists('model/Produto.php')) {
    require_once 'model/Produto.php';
} else {
    require_once '../model/Produto.php';
}
if (isset($_POST["id"])) {
    ProdutoController::adicionarAoCarrinho($_POST["id"]);
} else if (isset($_POST["idExcluir"])) {
    ProdutoController::removerDoCarrinho($_POST["idExcluir"]);
} else if (isset($_POST["idComprar"])) {
    ProdutoController::comprarProduto($_POST["idComprar"]);
}

class ProdutoController {

    public static function adicionarAoCarrinho($id) {

        $array = self::carregarProduto($id);

        if (isset($_SESSION["carrinho"])) {
            foreach ($_SESSION["carrinho"] as $value) { //verifica se o produto já não está no carrinho
                if ($value[0] == $id) {
                    header("location: ../telaInicial.php");
                    exit();
                }
            }
            foreach ($array as $value) {
                $_SESSION["carrinho"][] = array($value[0], $value[1], $value[2], $value[3], $value[4], $value[5], $value[6], $value[7]);
            }
        } else {
            $_SESSION["carrinho"] = $array;
        }

        header("location: ../telaCarrinho.php");
    }

    public static function removerDoCarrinho($id) {
        $array = $_SESSION["carrinho"];
        print_r($array);
        $x = 0;
        foreach ($array as $value) {
            if ($value[0] == $id) {
                $chave = array_search($value, $array);
            }
            $x++;
        }
        unset($array[$chave]);
        $_SESSION["carrinho"] = $array;
        header("location: ../telaInicial.php");
    }

    public static function comprarProduto($id) {
        $array = self::carregarProduto($id);
        foreach ($array as $value) {
            $_SESSION["sweet"] = "A compra do produto $value[1] custou $value[5] reais! Verifique seu Email após essa compra.";
        }
        mail ($_SESSION["usuario"][2] , 'Compra do produto' , $_SESSION["sweet"]);
        self::removerDoCarrinho($id);
        header("location: ../telaCarrinho.php");
    }

    public static function carregarProduto($id) {
        $produtoModel = new Produto();
        return $produtoModel->load($id);
    }

    public static function mostrarTodosProdutos() {

        $produtoModel = new Produto();
        return $produtoModel->mostrarTodosProdutos();
    }

}
