
<?php
require_once 'controller/ProdutoController.php';

if (!session_id()) {
  session_start();
}
?>

<html>
<head>
  <meta charset="UTF-8">
  <title>Tela Inicial</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>


</head>

<script>
$(document).ready(function () {
  $(".dropdown-button").dropdown();
  $(".button-collapse").sideNav();

});
</script>

<body>

  <?php
  if (isset($_SESSION['usuario'])) {
    $array = $_SESSION['usuario'];
  } else {
    header("location: index.php");
  }
  ?>
  <nav>
    <div class="nav-wrapper light-blue darken-4">
      <a class="brand-logo"><img src="imagens/Hunos.PNG" class="circle" style="height: 60px;"/></a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <?php if (isset($_SESSION["carrinho"])): ?> <li style="position: absolute; left:90%; border-radius: 50%;"><span class="new badge"><?php
        echo count($_SESSION["carrinho"]);
      endif;
      ?></span></li>
      <li><a href="telaCarrinho.php"><i class="material-icons">child_friendly</i></a></li>
      <li><a href="controller/logout.php">SAIR</a></li>
    </ul>
  </div>
</nav>


<BR><center><h3>Olá <?php echo $array[0][1]; ?>!</h3><h5>Estes são os produtos disponíveis na loja:</h5></center><BR>


  <div class="row">
    <div class="container">
      <?php foreach (ProdutoController::mostrarTodosProdutos() as $value): ?>
        <div class="col s12 m4">
          <div class="card">
            <div class="card-image" style="height: 300px; margin: 10px;">

              <img src="<?php echo $value[6]; ?>" style="height: 200px; width: 100%;">
              <span class="card-title black-text"><?php echo $value[2]; ?></span>
            </div>
            <div class="card-content">
              <p><b>Nome do produto:</b> <?php echo $value[1]; ?></p>
              <p><b>Breve descrição:</b> <?php echo $value[4]; ?></p>
              <p><b>Preço:</b> <?php echo $value[5]; ?></p>
            </div>
            <div class="card-action">
              <form method="POST" action="telaDetalhes.php">
                <input type="hidden" value="<?php echo $value[0]; ?>" name="id1"/>
                <button class="btn waves-effect waves-light center col s12" type="submit">Ver detalhes</button>
              </form><br>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>

  <BR>

    <footer class="page-footer light-blue darken-4">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Footer Huno</h5>
            <p class="grey-text text-lighten-4">Esses são os nosso patrocinadores:</p>
          </div>
          <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Entre em contato</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="#!">viegasgabriel156@gmail.com</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">augustofalcaoflach@gmail.com</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
          © 2018 Copyright Text
        </div>
      </div>
    </footer>

  </body>
  </html>
