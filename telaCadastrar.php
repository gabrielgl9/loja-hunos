<?php
if (!session_id()) {
    session_start();
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Cadastro de Usuário</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    </head>
    <body>
        <nav>
            <div class="nav-wrapper light-blue darken-4">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Apresentação</a>
                    <a href="telaCadastrar.php" class="breadcrumb">Cadastro</a>
                </div>
            </div>
        </nav>
    <center><h4>Cadastre-se no sistema!</h4></center><br>

    <div class="row">
        <div class="col s4 offset-s4">
            <div class="card">
                <div class="container">
                    <form method="post" action="controller/UsuarioController.php">
                        <input type="hidden" value="cadastro" name="cadastro"/>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="login" type="text" name="login" maxlength="20" required class="validate">
                                <label for="login">Nome de usuário</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email" name="email" maxlength="50" required class="validate">
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" type="password" name="senha" maxlength="60" required class="validate">
                                <label for="password">Password</label>
                            </div>
                            <a href="telaLogin.php" style="margin-left: 10px;">Já possui uma conta?</a>

                        </div>
                        <center>
                            <button class="btn center waves-effect waves-light" type="submit">Cadastrar Usuário
                                <i class="material-icons right">send</i>
                            </button>
                        </center>
                    </form> 
                    <br>
                </div>
            </div>
        </div>
    </div>


    <footer class="page-footer light-blue darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Footer Huno</h5>
                    <p class="grey-text text-lighten-4">Esses são os nosso patrocinadores:</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Entre em contato</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">viegasgabriel156@gmail.com</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">augustofalcaoflach@gmail.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2018 Copyright Text
            </div>
        </div>
    </footer>

    <?php
    if (isset($_GET['resultado']) and $_SESSION['sweet'] == "Erro ao cadastrar!") {
        echo "<script> swal('" . $_SESSION['sweet'] . "', 'Já existe um usuário com este nome', 'error');</script>";
        $_SESSION['sweet'] = null;
    }
    ?>
</body>
</html>
