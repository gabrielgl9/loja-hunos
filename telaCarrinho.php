<?php
require_once 'controller/ProdutoController.php';
if (!session_id()) {
    session_start();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Seu Carrinho</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    </head>
    <body>
        <nav>
            <div class="nav-wrapper light-blue darken-4">
                <div class="col s12">
                    <a href="telaInicial.php" class="breadcrumb">Loja</a>
                    <a href="telaCarrinho.php" class="breadcrumb">Carrinho</a>
                </div>
            </div>
        </nav>
        <?php if (isset($_SESSION["carrinho"]) and ! empty($_SESSION["carrinho"])):
            ?><center><h3>Produtos no Carrinho</h3></center>
        <?php endif; ?>
    <div class="row">
        <div class="container">
            <?php
            if (isset($_SESSION["carrinho"]) and ! empty($_SESSION["carrinho"])):
                $array = $_SESSION["carrinho"];
                foreach ($array as $value):
                    ?>
                    <div class="col s12 m4">
                        <div class="card">
                            <div class="card-image" style="height: 300px; margin: 10px;">
                                <img src="<?php echo $value[6]; ?>" class="circle" style="height: 200px; width: 100%;">
                                <span class="card-title black-text"><?php echo $value[2]; ?></span>
                            </div>
                            <div class="card-content">
                                <p>Preço: <?php echo $value[5]; ?> R$</p>
                            </div>
                            <form method="POST" action="controller/ProdutoController.php">
                                <input type="hidden" value="<?php echo $value[0]; ?>" name="idComprar"/>
                                <button class="btn waves-effect waves-light center col s12" type="submit">Comprar Produto</button>
                            </form>
                            <form method="POST" action="controller/ProdutoController.php">
                                <input type="hidden" value="<?php echo $value[0]; ?>" name="idExcluir"/>
                                <button class="btn waves-effect waves-light center col s12 red" type="submit">Remover do carrinho</button>
                            </form>
                        </div>
                    </div>
                    <?php
                endforeach;
            else:
                echo "<center><b><h1>Não há produtos em seu carrinho.</h1></b></center>";
                echo "<img src='imagens/CarrinhoVazio.jpg'/>";
            endif;
            ?>
        </div>
    </div><br>

    <footer class="page-footer light-blue darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Footer Huno</h5>
                    <p class="grey-text text-lighten-4">Esses são os nosso patrocinadores:</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Entre em contato</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">viegasgabriel156@gmail.com</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">augustofalcaoflach@gmail.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2018 Copyright Text
            </div>
        </div>
    </footer>

    <?php
    if (isset($_SESSION['sweet']) and !empty($_SESSION['sweet'])) {
        echo "<script> swal('" . $_SESSION['sweet'] . "', 'Compra realizada com sucesso', 'success');</script>";
        $_SESSION['sweet'] = null;
    }?>
</body>
</html>