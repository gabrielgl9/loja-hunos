<?php
require_once 'controller/ProdutoController.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Detalhes do Produto</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    </head>
    <body>
        <nav>
            <div class="nav-wrapper light-blue darken-4">
                <div class="col s12">
                    <a href="telaInicial.php" class="breadcrumb">Loja</a>
                    <a href="telaDetalhes.php" class="breadcrumb">Detalhes</a>
                </div>
            </div>
        </nav>

    <center><h3>Detalhes do Produto</h3></center>
    <div class="row">
        <div class="container">
            <div class="col s12 m4">
                <div class="card">
                    <?php foreach (ProdutoController::carregarProduto($_POST["id1"]) as $value): ?>
                        <div class="card-image" style="height: 300px; margin: 10px;">
                            <img src="<?php echo $value[6]; ?>" class="circle" style="height: 200px; width: 100%;">
                            <span class="card-title black-text"><?php echo $value[2]; ?></span>
                        </div>
                        <div class="card-content">
                            <p><b>Descrição:</b> <?php echo $value[3]; ?></p>
                            <p><b>Preço:</b> <?php echo $value[5]; ?> R$.</p>
                        </div>
                        <form method="POST" action="controller/ProdutoController.php">
                            <input type="hidden" value="<?php echo $_POST["id1"]; ?>" name="id"/>
                            <button class="btn waves-effect waves-light center col s12" type="submit">Adicionar ao carrinho</button>
                        </form>
                        <a href="telaInicial.php" class="btn waves-effect waves-light red" style="width: 100%;">Cancelar</a><br>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
    </div>

    <footer class="page-footer light-blue darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Footer Huno</h5>
                    <p class="grey-text text-lighten-4">Esses são os nosso patrocinadores:</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Entre em contato</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">viegasgabriel156@gmail.com</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">augustofalcaoflach@gmail.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2018 Copyright Text
            </div>
        </div>
    </footer>
</body>
</html>