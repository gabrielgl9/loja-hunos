<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Trabalho</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>

        <nav>
            <div class="nav-wrapper light-blue darken-4">
                <a class="brand-logo"><img src="imagens/Hunos.PNG" class="circle" style="height: 60px;"/></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="telaLogin.php" class="waves-effect waves-light btn">Entrar <i class="material-icons right"> directions_run </i></a></li>
                    <li><a href="telaCadastrar.php" class="waves-effect waves-light btn">Cadastrar <i class="material-icons right">person_add</i></a></li>
                </ul>
            </div>
        </nav>
    <center><h1>Dos Criadores da saga Hunos</h1></center><br><br>
    <div class="container">
        <div class="section">

            <!--   Icon Section   -->
            <div class="row">
                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center green-text"><i class="material-icons large">public</i></h2>
                        <h5 class="center">O mundo inteiro</h5>

                        <p class="light">Esta loja vitual é voltada para todo público, unindo diversos continentes, que presenciaram a obra "Os hunos" de Daniel Garbin, Jessé Lucas, Augusto Falcão, Gabriel Viegas e Pietro Carrara.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center red-text"><i class="material-icons large">thumb_up</i></h2>
                        <h5 class="center">Produtos Legais</h5>

                        <p class="light">Sabe-se que muitos sites vendem produtos ilegais, mas não é o caso deste. Pense o seguinte: se fossem ilegais, seriíamos preso. ah, nem sei mais o que estou escrevendo, acho que é só pra encher linguiça.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center blue-text"><i class="material-icons large">settings</i></h2>
                        <h5 class="center">Fácil de comprar</h5>

                        <p class="light">Para comprar um produto, existe um pequeno processo. Inicialmente haverá uma vitrine, que ao clicar em "detalhes" de um produto que demonstrou interesse, será exibido todas informações e, caso queira comprar, basta adicionar em seu carrinho, para que posteriormente possa comprar!</p></div>
                </div>
            </div>

        </div>
    </div><br><br><br>

    <div class="section">
        <div class="row">
            <div class="col s7 m12 l2 offset-m3 offset-l2" style="width: 300px;">
                <div class="image-container">
                    <img src="imagens/Gabriel.jpg" class="circle responsive-img">
                </div>
            </div>
            <div class="col s7 l4 m6 offset-m3">
                <h5>Gabriel Viégas da Silva</h5>
                <p>Desenvolvedor da plataforma e estudante do Instituto Federal do Rio Grande do Sul, campus canoas, atualmente no 4° ano de informática.</p>
            </div>
        </div><br><br>
        <div class="row">
            <div class="col s7 m12 l2 offset-m3 offset-l2" style="width: 300px;">
                <div class="image-container">
                    <img src="imagens/Augusto.jpg" class="circle responsive-img">
                </div>
            </div>
            <div class="col s7 l4 m6 offset-m3">
                <h5>Augusto Falcão Flach</h5>
                <p>Desenvolvedor da plataforma e estudante do Instituto Federal do Rio Grande do Sul, campus canoas, atualmente no 4° ano de informática.</p>
            </div>
        </div>
    </div>
</div>
<br><br><br>


<footer class="page-footer light-blue darken-4">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Footer Huno</h5>
                <p class="grey-text text-lighten-4">Esses são os nosso patrocinadores:</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Entre em contato</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="#!">viegasgabriel156@gmail.com</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">augustofalcaoflach@gmail.com</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Text
        </div>
    </div>
</footer>

</body>
</html>
