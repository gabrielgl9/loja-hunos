<?php
if (!session_id()) {
    session_start();
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Autenticação no Sistema</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    <body>
        <nav>
            <div class="nav-wrapper light-blue darken-4">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Apresentação</a>
                    <a href="telaLogin.php" class="breadcrumb">Autenticação</a>
                </div>
            </div>
        </nav>
    <center><h4>Entre na plataforma oficial dos Hunos</h4></center><br>

    <div class="row">
        <div class="col s6 m6 l4 offset-s3 offset-m3 offset-l4">
            <div class="card">
                <div class="card-image">
                    <img src="imagens/Login2.jpg" style="height:200px;">
                </div>
                <form method="POST" action="controller/UsuarioController.php" enctype="multipart/form-data" style="margin:10px;">
                    <input type="hidden" value="autenticar" name="autenticar"/>
                    <div class="row">
                        <div class="input-field col s12">
                            <!-- <i class="material-icons prefix pt-5">person_outline</i> -->
                            <input type="text" name="login" id="login">
                            <label for="login" class="center-align" data-error="wrong" data-success="right" required>
                                Login
                            </label> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <!-- <i class="material-icons prefix pt-5">lock_outline</i>-->
                            <input type="password"  name="senha" id="senha">
                            <label class="center-align" for="senha" data-error="wrong" data-success="right">
                                Senha
                            </label> 
                        </div>
                        <a href="telaCadastrar.php" style="margin-left: 10px;">Ainda não se cadastrou?</a>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light col s12">Entrar</button>                           

                            <br><br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="page-footer light-blue darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Footer Huno</h5>
                    <p class="grey-text text-lighten-4">Esses são os nosso patrocinadores:</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Entre em contato</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">viegasgabriel156@gmail.com</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">augustofalcaoflach@gmail.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2018 Copyright Text
            </div>
        </div>
    </footer>

    <?php
    if (isset($_GET['resultado']) and $_SESSION['sweet'] == "Erro ao realizar a autenticação!") {
        echo "<script> swal('" . $_SESSION['sweet'] . "', 'Não há nenhum usuário com este login ou senha', 'error');</script>";
        $_SESSION['sweet'] = null;
    }
    ?>
</body>
</html>
